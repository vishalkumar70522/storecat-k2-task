import {app, NODE_MODE, TASK_ID} from './init';

import * as cron from 'node-cron';
import {namespaceWrapper, INode} from './namespaceWrapper';
import * as nacl from 'tweetnacl';
import * as bs58 from 'bs58';
import * as crypto from 'crypto';
import * as web3 from '@_koi/web3.js';
import * as puppeteer from 'puppeteer';
import * as cheerio from 'cheerio';

const NODE_MODE_SERVICE = 'service';
let taskAccountInfo;
let submitterAccountKeyPair;
let submitterPubkey;
let connection;
let round;
let voteStatus;
let stakeStatus;

//  const NODE_MODE_SERVICE = 'service';

/**
 * @description Setup function is the first  function that is called in executable to setup the node
 */

async function setup(): Promise<any> {
  console.log('RUNNING THE StoreCat TASK');
  const submitterAccountKeyPair = await namespaceWrapper.getSubmitterAccount();
  if (submitterAccountKeyPair == null) {
    throw 'Staking Wallet do not exists';
  }
  await namespaceWrapper.defaultTaskSetup();
}

/*
 * @description Using to validate an individual node.
 */
async function validateNode(node): Promise<boolean> {
  // Write the logic for checking the submissions here and if the submissions is okay then call the return the vote boolean
  let vote;
  console.log('SUBMISSION VALUE', node.submission_value);
  const doodle = node.submission_value;
  console.log('URL', doodle);

  // check the google doodle

  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto('https://www.google.com/doodles');
  let bodyHTML = await page.evaluate(() => document.documentElement.outerHTML);
  const $ = cheerio.load(bodyHTML);

  let scrapedDoodle = $('.latest-doodle.on').find('div > div > a > img').attr('src');
  if (scrapedDoodle.substring(0, 2) == '//') {
    scrapedDoodle = scrapedDoodle.substring(2, scrapedDoodle.length);
  }
  console.log({scrapedDoodle});

  // vote based on the scrapedDoodle

  try {
    if (scrapedDoodle == doodle) {
      vote = true;
    } else {
      vote = false;
    }
  } catch (e) {
    console.error(e);
    vote = false;
  }
  browser.close();
  return vote;
}

async function submitTask() {
  const browser = await puppeteer.launch();
  const page = await browser.newPage();
  await page.goto('https://www.google.com/doodles');
  let bodyHTML = await page.evaluate(() => document.documentElement.outerHTML);
  const $ = cheerio.load(bodyHTML);

  let scrapedDoodle = $('.latest-doodle.on').find('div > div > a > img').attr('src');
  if (scrapedDoodle.substring(0, 2) == '//') {
    scrapedDoodle = scrapedDoodle.substring(2, scrapedDoodle.length);
  }
  console.log({scrapedDoodle});

  console.log('SUBMISSION VALUE', scrapedDoodle);
  await namespaceWrapper.checkSubmissionAndUpdateRound(scrapedDoodle);
}
/*
 * @description Using to validate an individual node.
 */

/**
 * @description Execute function is called just after the setup function  to run Submit, Vote API in cron job
 */
async function execute(): Promise<void> {
  console.log('RUNNING');
  console.log('NODE MODE', NODE_MODE);
  if (NODE_MODE == NODE_MODE_SERVICE) {
    //  Submission
    cron.schedule('*/120 * * * *', () => submitTask());
  }
  // Voting
  cron.schedule('*/40 * * * *', () => {
    namespaceWrapper.validateAndVoteOnNodes(validateNode);
  });
}

setup().then(execute);
